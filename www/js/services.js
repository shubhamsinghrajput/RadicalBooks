angular.module('starter.services', [])

        .controller('StepData', ['irkResults', '$http', '$scope', '$location', function (irkResults, $http, $scope, $location) {

                $scope.results = irkResults.getResults();

                $scope.SendData = function () {
                    var formData = $scope.results;
                    var postData = 'categories=' + JSON.stringify(formData);

                    $http({
                        method: 'POST',
                        url: 'http://localhost/radical_survey/call_services.php',
                        data: postData,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}

                    }).then(function successCallback(response) {
                        $scope.responsedata = response.data;
                        alert(response.data.val);
                    }, function errorCallback(response) {
                        alert(response.status);
                    });
                    $location.path("/SuccessPage");
                };
            }])

        .controller('ViewData', ['$http', '$scope', '$location', '$ionicModal', function ($http, $scope, $location, $ionicModal) {

                $scope.viewModalConsent = function () {
                    var formViewData = {'loginid': '1', 'action': 'view'};
                    var postViewData = 'categories=' + JSON.stringify(formViewData);

                    $http({
                        method: 'POST',
                        url: 'http://localhost/radical_survey/call_services.php',
                        data: postViewData,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}

                    }).then(function successCallback(response) {
                        $scope.responsedata = response.data;
                        alert(response.data);
                    }, function errorCallback(response) {
                        alert(response.status);
                    });

                    // $location.path("/SuccessPage");
                };
				
				$scope.doViewExit = function () {
					alert('helll');
					navigator.app.exitApp();
					$location.path("/SuccessPage");
				}

            }])